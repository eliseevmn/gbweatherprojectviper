import UIKit

class CustomRefreshController: UIRefreshControl {
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .white
        tintColor = .black
        self.frame = CGRect.init(x: 0, y: 0, width: 50, height: 50)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
