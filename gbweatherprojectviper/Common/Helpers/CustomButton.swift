import UIKit

class CustomButton: UIButton {

    let widthAndHeight: CGFloat
    var image: UIImage

    init(widthAndHeight: CGFloat, image: UIImage) {
        self.widthAndHeight = widthAndHeight
        self.image = image
        super.init(frame: .zero)
        
        widthAnchor.constraint(equalToConstant: widthAndHeight).isActive = true
        heightAnchor.constraint(equalToConstant: widthAndHeight).isActive = true
        layer.cornerRadius = widthAndHeight/2
        layer.masksToBounds = true
        setImage(image, for: .normal)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
