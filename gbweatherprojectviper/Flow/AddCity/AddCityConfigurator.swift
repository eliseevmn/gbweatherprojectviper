import Foundation

protocol AddCityConfiguratorProtocol: class {
    func configure(with viewController: AddCityViewController, delegate: MyCityListPresenterProtocol)
}

class AddCityConfigurator: AddCityConfiguratorProtocol {

    func configure(with viewController: AddCityViewController, delegate: MyCityListPresenterProtocol) {
        let presenter = AddCityPresenter(view: viewController)
        let interactor = AddCityInteractor(presentor: presenter)
        let router = AddCityRouter()
        
        viewController.presentor = presenter
        presenter.router = router
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.delegate = delegate
    }
}
