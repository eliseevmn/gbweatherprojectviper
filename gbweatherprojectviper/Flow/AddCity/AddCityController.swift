import UIKit

protocol AddCityViewProtocol {
    func reloadData()
    var returnCity: ((CityResponse) ->())? { get set }
}

class AddCityViewController: UIViewController {
    
    var presentor: AddCityPresenterProtocol!
    
    private var tableView: UITableView = UITableView(frame: .zero, style: .plain)
    private let searchController = UISearchController(searchResultsController: nil)
    private var timer: Timer?
    var returnCity: ((CityResponse) ->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
        navigationController?.navigationBar.isHidden = false
        
        setupUI()
        setupSearchController()
    }
    
    private func setupUI() {
        view.addSubview(tableView)
        tableView.anchor(top: view.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: view.bottomAnchor,
                         trailing: view.trailingAnchor)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
        tableView.register(AddCityTableViewCell.self, forCellReuseIdentifier: AddCityTableViewCell.reuseId)
        
        navigationItem.title = "Поиск"
    }
    
    private func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.placeholder = "Search City"
        searchController.searchBar.delegate = self
    }
}

extension AddCityViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentor.citiesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AddCityTableViewCell.reuseId, for: indexPath) as! AddCityTableViewCell
        
        presentor.selectRow(at: indexPath)
        cell.configure(city: presentor.myCity)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presentor.selectRow(at: indexPath)
        presentor.returnToListController()
    }
}

extension AddCityViewController: UISearchBarDelegate, UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let query = searchController.searchBar.text, query.count > 2 else {
            return
        }
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { [weak self] (_) in
            self?.presentor.searchCity(searchCity: query)
            self?.reloadData()
        })
        
    }
}

extension AddCityViewController: AddCityViewProtocol {
    func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
