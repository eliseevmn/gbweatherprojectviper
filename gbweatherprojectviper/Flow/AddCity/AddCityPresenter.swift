import UIKit

protocol AddCityPresenterProtocol: class {
    var myCity: CityResponse? { get }
    var citiesCount: Int { get }
    var returnCity: ((CityResponse) ->())? { get set }
    init(view: AddCityViewProtocol)
    func searchCity(searchCity: String)
    func selectRow(at indexPath: IndexPath)
    func returnToListController()
}

class AddCityPresenter: AddCityPresenterProtocol {
    
    var view: AddCityViewProtocol
    var interactor: AddCityInteractorProtocol!
    var router: AddCityRouterProtocol!
    var delegate: MyCityListPresenterProtocol?
    
    private var citiesList: [CityResponse] = []
    private var indexPath: IndexPath?
    var returnCity: ((CityResponse) ->())?
    
    var citiesCount: Int {
        return citiesList.count
    }
    
    var myCity: CityResponse? {
          guard let indexPath = indexPath else { return nil }
          return getCity(at: indexPath)
      }
    
    required init(view: AddCityViewProtocol) {
        self.view = view
    }
    
    func selectRow(at indexPath: IndexPath) {
        self.indexPath = indexPath
    }
    
    func searchCity(searchCity: String) {
        interactor.fetchWeatherCities(searchCity: searchCity)
    }
    
    func returnToListController() {
        guard let city = self.myCity else { return }
        self.delegate?.didAddCity(city: city)
        router.returnToMyCityListController(from: view)
    }
    
    private func getCity(at indexPath: IndexPath) -> CityResponse? {
        if citiesList.indices.contains(indexPath.row) {
            return citiesList[indexPath.row]
        }
        return nil
    }
}

extension AddCityPresenter: AddCityInteractorOutputProtocol {
    func citiesDidReceive(searchCity: [CityResponse]) {
        self.citiesList = searchCity
        view.reloadData()
    }
}
