import Foundation

protocol MyCityListInteractorProtocol: class {
    init(presenter: MyCityListInteractorOutputProtocol)
    func fetchWeatherCities(cityName: String)
}

protocol MyCityListInteractorOutputProtocol: class {
    func citiesWeatherDidReceive(_ cities: [MyCity])
}

class MyCityListInteractor: MyCityListInteractorProtocol {
    
    unowned var presenter: MyCityListInteractorOutputProtocol
    private(set)var cities: [MyCity] = []
    
    required init(presenter: MyCityListInteractorOutputProtocol) {
        self.presenter = presenter
    }
    
    func fetchWeatherCities(cityName: String) {
        NetworkService.sendRequestForWeather(router: .currentWeather(cityName: cityName)) { [weak self] (currentWeather: CurrentWeatherResponse) in
            
            NetworkService.sendRequestForWeather(router: .fiveDailyWeather(cityName: cityName)) { (dailyWeather: DailyWeatherResponse) in
                
                let myCity = MyCity(name: cityName, dailyWeather: dailyWeather, currentWeather: currentWeather)
                
                if self?.cities.contains(where: { (city) -> Bool in
                    return city.name.contains(myCity.name)
                }) ?? false {
                    if let row = self?.cities.firstIndex(where: {$0.name == myCity.name}) {
                        self?.cities[row].currentWeather = myCity.currentWeather
                    }
                    self?.presenter.citiesWeatherDidReceive(self?.cities ?? [])
                } else {
                    self?.cities.append(myCity)
                    self?.presenter.citiesWeatherDidReceive(self?.cities ?? [])
                }
            }
        }
    }
}
