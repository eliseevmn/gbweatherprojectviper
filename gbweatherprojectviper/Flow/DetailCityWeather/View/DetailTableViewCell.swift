import UIKit

class DetailTableViewCell: UITableViewCell {
    
    static let reuseId = "DetailTableViewCell"
    
    private let dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        return label
    }()
    
    private var iconWeather: WebImageView = {
        let imageView = WebImageView()
        imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        return imageView
    }()
    
    private var temperatureLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        return label
    }()
    
    private var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return label
    }()
    
    private var dateFormatter: DateFormatter = {
        let dt = DateFormatter()
        dt.dateFormat = "HH:mm EEEE"
        return dt
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        configureUI()
    }
    
    private func configureUI() {
        let verticalStackView = UIStackView(arrangedSubviews: [
            dateLabel,
            temperatureLabel
        ])
        verticalStackView.axis = .vertical
        verticalStackView.alignment = .leading
        verticalStackView.spacing = 10
        let horizontalStackView = UIStackView(arrangedSubviews: [
            iconWeather,
            verticalStackView,
            UIView(),
            descriptionLabel
        ])
        horizontalStackView.axis = .horizontal
        horizontalStackView.spacing = 10
        horizontalStackView.alignment = .center
        addSubview(horizontalStackView)
        horizontalStackView.anchor(top: topAnchor,
                                   leading: leadingAnchor,
                                   bottom: bottomAnchor,
                                   trailing: trailingAnchor,
                                   padding: .init(top: 10, left: 16, bottom: 10, right: 16))
    }
    
    func configure(with dailyWeather: DailyList?) {
        
        let timeInterval = Double(dailyWeather?.dt ?? 0)
        dateLabel.text = dateFormatter.string(from: Date(timeIntervalSince1970: timeInterval))
        
        let tempMaxFromJson = dailyWeather?.main.tempMax ?? 0.0
        let tempMinFromJson = dailyWeather?.main.tempMin ?? 0.0
        let tempMaxCelcium = String(format:"%.0f℃", tempMaxFromJson - 273.15)
        let tempMinCelcium = String(format:"%.0f℃", tempMinFromJson - 273.15)
        temperatureLabel.text = "Max: \(tempMaxCelcium), min: \(tempMinCelcium)"

        descriptionLabel.text = dailyWeather?.weather[0].main
        iconWeather.set(iconString:dailyWeather?.weather[0].icon)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
