import Foundation

struct WeatherDetailsData {
    var city: MyCity?
}

protocol DetailWeatherPresenterProtocol: class {
    var weatherList: Int { get }
    var dailyWeather: DailyList? { get }
    init(view: DetailWeatherViewProtocol)
    func showWeatherDetails()
    func selectRow(at indexPath: IndexPath)
}

class DetailWeatherPresenter: DetailWeatherPresenterProtocol {
    
    var view: DetailWeatherViewProtocol
    var interactor: DetailWeatherInteractorProtocol!
    var router: DetailWeatherRouterProtocol!
    
    var weatherList: Int {
        return dailyList.count
    }
    
    var dailyWeather: DailyList? {
        guard let indexPath = indexPath else { return nil }
        return getWeather(at: indexPath)
    }
    
    private var dailyList: [DailyList] = []
    private var indexPath: IndexPath?
    
    required init(view: DetailWeatherViewProtocol) {
        self.view = view
    }
    
    func showWeatherDetails() {
        interactor.provideWeatherDetails()
    }
    
    func selectRow(at indexPath: IndexPath) {
        self.indexPath = indexPath
    }

    private func getWeather(at indexPath: IndexPath) -> DailyList? {
        if dailyList.indices.contains(indexPath.row) {
            return dailyList[indexPath.row]
        }
        return nil
    }
}

extension DetailWeatherPresenter: DetailWeatherInteractorOutputProtocol {
    func receiveWeatherDetails(weatherDetails: WeatherDetailsData) {
        view.displayCurrentWeather(with: weatherDetails.city)
        
        guard let listWeather = weatherDetails.city?.dailyWeather?.list else { return }
        self.dailyList = listWeather
        view.reloadData()
    }
}
